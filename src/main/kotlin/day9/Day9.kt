package day9

import Utils.Companion.printIt
import Utils.Companion.readInput

sealed interface Direction {
    object Left : Direction {
        override fun toString(): String = "L"
    }
    object Right : Direction {
        override fun toString(): String = "R"
    }

    companion object {
        fun fromLine(line: String): List<Direction> = line.map {
            when(it){
                'L' -> Left
                'R' -> Right
                else -> throw IllegalStateException("Unknown letter : $it")
            }
        }
    }
}

typealias Position = String

data class MapNode(val position : Position, val leftDirection: Position, val rightDirection: Position){
    companion object {
        fun fromLine(line: String): MapNode {
            val (position, directions) = line.split(" = ")
            val (leftDirection, rightDirection) = directions.drop(1).dropLast(1).split(", ")

            return MapNode(position, leftDirection, rightDirection)
        }
    }
}

typealias Index = Int

data class DetectedLoop(val endsPassages: List<Index>, val loopSize: Int)

fun main() {
    val lines = ("/day9/input").readInput { it }

    val (lrSize, lrSequence) = Direction.fromLine(lines.first()).let {
        it.size to it.loopInfinitely()
    }

    val mapWorld = lines.drop(2).map { MapNode.fromLine(it) }.associateBy { it.position }


    // part 1

    var currentPosition: Position = "AAA"
    for((index, instruction) in lrSequence.withIndex()){
//        println("$index $instruction")
        val nextPosition = mapWorld[currentPosition]!!.let {
            when(instruction){
                Direction.Left -> it.leftDirection
                Direction.Right -> it.rightDirection
            }
        }

        if(nextPosition == "ZZZ"){
            println("Found it ! It is ${index + 1} steps away") // Answer is 11309
            break
        }
        currentPosition = nextPosition
    }


    // second try of
    // part 2
    // but still not ok...

    val startPositions = mapWorld.map { it.value.position }.filter { it.endsWith("A") }
    val currentPositions = startPositions.toMutableList()
    var detectedLoops = startPositions.map { startPosition ->
        val endsPassages = mutableListOf<Index>()


        var runningPosition = startPosition
        for ((index, instruction) in lrSequence.withIndex()) {
            runningPosition = when (instruction) {
                Direction.Left -> mapWorld[runningPosition]!!.leftDirection
                Direction.Right -> mapWorld[runningPosition]!!.rightDirection
            }

            if (runningPosition.endsWith("Z")) {
                endsPassages.add(index)
            }

            if(runningPosition == startPosition){
                println("looooooooop detected for $startPosition")
            }

            if (runningPosition == startPosition && index % lrSize == 0) {
                println("loop detected for $startPosition")
                return@map DetectedLoop(endsPassages, index)
            }
        }
    }


    // try of part 2, but need optimization
//
//    for((index, instruction) in lrSequence.withIndex()){
//        if(index % 10_000_000 == 0) println("$index $instruction")
//        val nextPositions =
//            when(instruction){
//                Direction.Left -> currentPositions.map { mapWorld[it]!!.leftDirection }
//                Direction.Right -> currentPositions.map { mapWorld[it]!!.rightDirection }
//            }
//
//
////        currentPositions = nextPositions
//        if(currentPositions.all { it.endsWith("Z") }){
//            println("Found it ! It is ${index + 1} steps away") // Answer 149_412_502 is too low, 1_840_000_000 is too low
//            // got Exception in thread "main" java.lang.ArithmeticException: Index overflow has happened.
//            break
//        }
//
//        currentPositions.forEachIndexed() { i, currentPosition ->
//            if(currentPosition == startPositions[i] && index % lines.first().length == 0 && loopIndexes[i] == 0){
////                println("loop detected for $currentPosition")
//                loopIndexes[i] = index
//            }
//
//            if(loopIndexes.all { it != 0 }){
//                println("Got all the loop")
//                break
//            }
//        }
//    }

}

private fun <E> List<E>.loopInfinitely() : Iterator<E> {
    return object : Iterator<E> {
        var index = 0
        override fun hasNext(): Boolean = true

        override fun next(): E {
            val element = this@loopInfinitely[index]
            index = (index + 1) % this@loopInfinitely.size
            return element
        }
        }
}




