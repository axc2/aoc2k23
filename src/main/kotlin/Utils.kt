import java.io.File


class Utils {

    companion object {
//        fun String.readResource(): String = object {}.javaClass.getResource(this)?.readText() ?: ""

        // TODO non sense to have that as an extension function
        fun <R> String.readInput(map: (String) -> R = { i -> i as R }): List<R> =
            File(object {}.javaClass.getResource(this)!!.file)
                .readLines()
                .map(map)

        fun <T> T.printIt(): T {
            println(this)
            return this
        }
    }

}