package day5

import Utils.Companion.printIt
import Utils.Companion.readInput

private val List<String>.sections: List<List<String>>
        get() = joinToString("\n").split("\n\n").map { split -> split.split("\n") }

data class MapRange(val sourceStart: Long, val destinationStart: Long, val rangeWidth: Long) {
    fun contains(index: Long): Boolean = index in sourceStart until (sourceStart + rangeWidth)

    //assume we are in the range
    fun transform(input: Long): Long = input + (destinationStart - sourceStart)
}

data class Mapping(val sourceSection: String, val targetSection: String, val mapping: List<MapRange>) {
    fun transform(input: Long): Long {
        //assume at most 1 compatible range exist
        val compatibleRange = mapping.find { it.contains(input) }
        if (compatibleRange != null) {
            return compatibleRange.transform(input)
        }
        return input
    }
}

fun main() {
    val lines = ("/day5/input").readInput { it }
    val sections = lines.sections

    val seeds = sections[0][0].split(": ")[1].split(" ").map { it.toLong() }

    val mappings = sections.drop(1).map {
        Mapping(
            sourceSection = it[0].split("-")[0],
            targetSection = it[0].split("-")[2].split(" ")[0],
            mapping = it.drop(1)
                .map { line -> line.split(" ").map { it.toLong() } }
                .map { MapRange(it[1], it[0],  it[2]) }
        )
    }

    // Part 1
    seeds.map {  seed ->
        val resultOfTransformations = mappings.fold(seed) { acc, mapping ->
            mapping.transform(acc)
        }

        resultOfTransformations
    }.min().printIt() //51580674


    // Part 2
    seeds.asSequence()
        .windowed(2, step = 2)
        .map {  (startRange, rangeLength) ->
            startRange until startRange + rangeLength
        }
        .flatten() //if it is not a sequence, more than 8.4 GB memory allocated here :-o (1680883088*64bits = 13GB)
        .map {  seed ->
            // same code as part 1 below
            val resultOfTransformations = mappings.fold(seed) { acc, mapping ->
                mapping.transform(acc)
            }

            resultOfTransformations
        }.min().printIt()
        //99751241 is too high (fixed then until instead of '..' in day5.MapRange.contains)
        //99751240 is the good answer :)
}

//TODO try to have bruteforce and parallel streams (else CPU 12.5%)
//TODO try optimized way (with boundaries)
