package day7

import Utils.Companion.printIt
import Utils.Companion.readInput

enum class HandFigure(val value: Int){
    FIVE_OF_A_KIND(7),
    FOUR_OF_A_KIND(6),
    FULL_HOUSE(5),
    THREE_OF_A_KIND(4),
    TWO_PAIRS(3),
    ONE_PAIR(2),
    HIGH_CARD(1)
}

// A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, or 2
fun compareCards(cardA: Char, cardB: Char): Int {
    val cardAValue = when(cardA){
        'A' -> 14
        'K' -> 13
        'Q' -> 12
        'J' -> 11
        'T' -> 10
        else -> cardA.toString().toInt()
    }

    val cardBValue = when(cardB){
        'A' -> 14
        'K' -> 13
        'Q' -> 12
        'J' -> 11
        'T' -> 10
        else -> cardB.toString().toInt()
    }

    return cardAValue.compareTo(cardBValue)
}

data class Hand(val cards: List<Char>) : Comparable<Hand> {
    fun getFigure(): HandFigure {
        val groupedCards = cards.groupBy { it }
        val numberOfCardsPerFigure = groupedCards.map { it.value.size }.sorted()

        return when(numberOfCardsPerFigure){
            listOf(1, 1, 1, 1, 1) -> HandFigure.HIGH_CARD
            listOf(1, 1, 1, 2) -> HandFigure.ONE_PAIR
            listOf(1, 2, 2) -> HandFigure.TWO_PAIRS
            listOf(1, 1, 3) -> HandFigure.THREE_OF_A_KIND
            listOf(2, 3) -> HandFigure.FULL_HOUSE
            listOf(1, 4) -> HandFigure.FOUR_OF_A_KIND
            listOf(5) -> HandFigure.FIVE_OF_A_KIND
            else -> throw IllegalStateException("Unknown figure")
        }
    }

    override fun compareTo(other: Hand): Int {
        val figure = getFigure()
        val otherFigure = other.getFigure()

        if(figure != otherFigure){
            return figure.value.compareTo(otherFigure.value)
        }

        //compare all cards, one by one, from the index 0
        for(index in cards.indices){
            val card = cards[index]
            val otherCard = other.cards[index]

            if(card != otherCard){
                return compareCards(card, otherCard)
            }
        }

        return 0 //are equal
    }
}

data class HandBid(val hand: Hand, val bid: Int)

fun main() {
    val dataInput = ("/day7/input").readInput { it }
        .asSequence()
        .map {
            val (handString, bidString) = it.split(" ")

            HandBid(
                hand = Hand(handString.toList()),
                bid = bidString.toInt()
            )
        }
        .toList()

    //sort dataInput
    val sortedDataInput = dataInput.sortedBy { it.hand }

    sortedDataInput.forEach {
        println("${it.hand} ${it.bid} ${it.hand.getFigure()}")
    }

    sortedDataInput.foldIndexed(0) { index, acc, handBid ->
        acc + (index + 1) * handBid.bid
    }.printIt()

}



