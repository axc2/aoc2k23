package day3

import Utils.Companion.printIt
import Utils.Companion.readInput

data class PartNumber(val number: Int, val location: Int){ // location is the index of the start in the line
    val width = number.toString().length
}

data class Scope(val beforeLine: String, val line: String, val afterLine: String){
    fun contourPart(startPart: Int, endPart: Int): Set<Char> {
        val contour = mutableSetOf<Char>()

        // left border
        if(startPart > 0){
            contour += beforeLine[startPart - 1]
            contour += line[startPart - 1]
            contour += afterLine[startPart - 1]
        }

        // top and bottom
        contour.addAll(beforeLine.slice(startPart..endPart).toSet())
        contour.addAll(afterLine.slice(startPart..endPart).toSet())

        // right border
        if(line.length > endPart -2){
            contour += beforeLine[endPart]
            contour += line[endPart]
            contour += afterLine[endPart]
        }

        return contour
    }


}

fun main() {
    val lines = ("/day3/input").readInput { it }

    val widthOfField = lines[0].removeSuffix("\n").length
    val emptyLine = ".".repeat(widthOfField)

    (listOf(emptyLine) + lines + listOf(emptyLine))
        .map { ".$it." } //TODO add that to the parser requirement // Is it really better to do that like that?
        .windowed(3, step = 1) {
            Scope(it[0], it[1], it[2])
        }
        .flatMap { scope ->
            val parts = scope.line.findPartNumbers()
            parts.map { part -> part to scope }
        }
        .map {  (part, scope) ->
            if(checkIfPartIsAdjacent(part, scope)) part.number else 0
        }
        .sum().printIt()

    // no time to do part 2
    // maybe an idea would be
    // to retrieve set of PartNumber to gear (location x/y)
    // would not be a lot to update
    // not very optimized although ?
}

fun checkIfPartIsAdjacent(part: PartNumber, scope: Scope): Boolean {
    val contour = scope.contourPart(part.location, part.location+part.width)

    return contour.any { !it.isDigit() && it != '.' }
}

private fun String.findPartNumbers(): List<PartNumber> {
    val ret = mutableListOf<PartNumber>()

    val partParser = PartParser()
    this.forEachIndexed { index, c ->
        partParser.step(index, c).also {
            if (it != null) ret.add(it)
        }
    }

    return ret.toList()
}


data class PartParser(private var onPart: Boolean, private var partNumber: Int, private var location: Int){

    constructor() : this(false, 0, 0)

    private fun startPart(index: Int, digitToInt: Int){
        onPart = true
        partNumber = digitToInt
        location = index
    }

    private fun addPart(c : Int){
        require(onPart) { "addPart can only be called when onPart is true" }

        partNumber = partNumber * 10 + c
    }

    private fun endPart(): PartNumber {
        require(onPart) { "endPart can only be called when onPart is true" }

        onPart = false
        return PartNumber(partNumber, location)
    }


    fun step(index : Int, c: Char) : PartNumber? {
        when {
            c.isDigit() && !onPart -> startPart(index, c.digitToInt())
            c.isDigit() && onPart -> addPart(c.digitToInt())
            !c.isDigit() && !onPart -> Unit // still not on a part
            !c.isDigit() && onPart -> return endPart()
        }

        return null
    }

    // for a new line you need to use a newly initialized PartParser

}


