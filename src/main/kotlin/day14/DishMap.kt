package day14

import java.lang.UnsupportedOperationException

typealias DishElement = Char

data class DishMap(val cells: Array<Array<DishElement>>) {

    constructor(sizeX: Int, sizeY: Int) : this(Array(sizeX) { Array(sizeY) { '.' } })

    val width: Int get() = cells[0].size
    val height: Int get() = cells.size

    operator fun get(x: Int, y: Int) = cells[y][x]
    operator fun set(x: Int, y: Int, value: DishElement) { cells[y][x] = value }


    // convenient way to scan the map in an other direction
//    val columns:




    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DishMap

        return cells.contentDeepEquals(other.cells)
    }

    override fun hashCode(): Int {
        return cells.contentDeepHashCode()
    }

    fun forEachColumn(action: (columnIndex: Int) -> Unit) {
        for (columnIndex in 0 until width) {
            action(columnIndex)
        }
    }


}