package day14

import Utils.Companion.printIt
import Utils.Companion.readInput



fun main() {
    val lines = ("/day14/input").readInput { it }

    val dishMap = DishMap(lines.first().length, lines.size)
    lines.forEachIndexed { y, line ->
        line.forEachIndexed { x, char ->
            dishMap[x, y] = char
        }
    }

    dishMap.forEachColumn { columnIndex ->
        var landingPoint = 0 // will be updated if cube/round rock is there
        (0 until dishMap.height).forEach { rowIndex ->
            val cell = dishMap[columnIndex, rowIndex]
            when (cell) {
                '.' -> Unit
                '#' -> landingPoint = rowIndex + 1
                'O' -> {
                    // make it fall (implicitely will not move if landing point is already there)
                    dishMap[columnIndex, rowIndex] = '.' //disapear at this place
                    dishMap[columnIndex, landingPoint] = 'O' //appear at this place
                    // update landing point
                    landingPoint += 1
                }
            }
        }
    }

//    dishMap.printAscii()

    dishMap.cells.mapIndexed { rowIndex, row ->
        val loadPerElement = dishMap.height - rowIndex
        val numberOfElement = row.count { it == 'O' }

        loadPerElement * numberOfElement
    }.sum().printIt() //answer was 113078



    // part2

    //unfortunately no time for it...


}

private fun DishMap.printAscii() {
    for (y in 0 until this.cells.size){
        for (x in 0 until this.cells[0].size){
            print(this[x, y])
        }
        println()
    }
}
