package day2

import Utils.Companion.printIt
import Utils.Companion.readInput
import kotlin.math.max

data class Pick(val color: String, val numberOfCubes: Int) {

}

data class PickSet(val set: Set<Pick>) {

}

data class Game(val gameNumber: Int, val pickSets: List<PickSet>) {

}

val ELF_GAME_MAX_CONDITION = mapOf(
    "red" to 12,
    "green" to 13,
    "blue" to 14
)

//
fun main() {

    val games = ("day2/input").readInput { line ->
        //ex: Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        // serialize that line
        val (gameId, gameContent) = line.split(": ")

        val gameNumber = gameId.split(" ")[1].toInt()

        gameContent.split("; ")
            .map { pickSet ->
                pickSet.split(", ")
                    .map { pick ->
                        val (numberOfCubes, color) = pick.split(" ")
                        Pick(color, numberOfCubes.toInt())
                    }.let {
                        PickSet(it.toSet())
                    }
            }.let {
                Game(gameNumber, it)
            }
    }

    // part 1
    games.map { game ->
        // check if the game would have been possible
        val possible = game.pickSets
            .all { pickSet ->
                pickSet.set.groupBy { it.color }
                    .all { (color, picks) ->
                        picks.sumOf { it.numberOfCubes } <= (ELF_GAME_MAX_CONDITION[color] ?: 0)
                    }
            }

        if (possible) game.gameNumber else 0
    }.sum().printIt() //right answer was 2204


    //part 2
    games.map { game ->
        val minimums = mutableMapOf(
            "red" to 0,
            "green" to 0,
            "blue" to 0
        )

        game.pickSets.forEach { pickSet ->
            pickSet.set.forEach { pick ->
                minimums[pick.color] = max(minimums[pick.color] ?: 0, pick.numberOfCubes)
            }
        }

        // multiply values
        val power = minimums.values.reduce { acc, i -> acc * i }
        power
    }.sum().printIt() //right answer was

}
