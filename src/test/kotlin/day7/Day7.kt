package day7

import day7b.Hand
import kotlin.test.Test

class Day7{

    @Test
    fun testComparison(){
        val fiveOfAKind = Hand(listOf('2', '2', '2', '2', '2'))
        val fourOfAKind = Hand(listOf('2', '2', '2', '2', '3'))
        val fullHouse = Hand(listOf('2', '2', '2', '3', '3'))
        val threeOfAKind = Hand(listOf('2', '2', '2', '3', '4'))
        val twoPairs = Hand(listOf('2', '2', '3', '3', '4'))
        val onePair = Hand(listOf('2', '2', '3', '4', '5'))
        val highCard = Hand(listOf('2', '3', '4', '5', '6'))

        assert(fiveOfAKind > fourOfAKind)
        assert(fourOfAKind > fullHouse)
        assert(fullHouse > threeOfAKind)
        assert(threeOfAKind > twoPairs)
        assert(twoPairs > onePair)
        assert(onePair > highCard)

    }

    @Test
    fun testComparisonSameFigure(){
        val fourOfAKindA = Hand(listOf('2', '2', '2', '2', '4'))
        val fourOfAKindB = Hand(listOf('3', '3', '3', '3', '4'))
        val fourOfAKindC = Hand(listOf('C', 'C', 'C', 'C', '2'))


        assert(fourOfAKindA < fourOfAKindB)
        assert(fourOfAKindB < fourOfAKindC)
    }

}