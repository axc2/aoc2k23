spelledNumbers = {
    "zero": 0,
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9
}


def createATreeForParsing(spelledToNumbers):
    return # TODO
    tree = {}
    for (key, value) in spelledNumbers:
        for char in key:
            pass  # TODO here need recursive


treeParsing = createATreeForParsing(spelledNumbers)


# tree is indexing the characters of the spelling to the value
# f -> o -> u -> r -> 4
#   -> i -> v -> e -> 5
# ...
# in another exercise with a bigger dict, would improve a lot the perf
# TODO what are the tree struct in Python ?


def tryParse(string, indexInWord):
    # TODO would have liked to use the tree
    for (possibleNumberKey, value) in spelledNumbers.items():
        if string[indexInWord:].startswith(possibleNumberKey):
            return str(value)


def retrieveDigits(string):
    # only keep digits from the string, even if in letters
    digits = ""
    for index in range(string.__len__()):
        if string[index].isdigit():
            digits += string[index]
        else:
            triedParsing = tryParse(string, index)
            if (triedParsing != None):
                digits += triedParsing

    return digits


def retrieveFirstAndLastDigit(string):
    digitsString = retrieveDigits(string)
    if len(digitsString) < 1:
        raise Exception("No digits found")
    elif len(digitsString) == 1:
        return int(digitsString[0] + digitsString[0])
    else:
        return int(digitsString[0] + digitsString[-1])


# Read input1

input1 = open("input1", "r")

# Read line by line in a loop

sum = 0
for line in input1:
    sum += retrieveFirstAndLastDigit(line)

print(sum)
