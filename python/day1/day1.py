
def retrieveDigits(string):
    # only keep digits from the string
    string = ''.join(filter(str.isdigit, string))
    return string

def retrieveFirstAndLastDigit(string):
    digitsString = retrieveDigits(string)
    if len(digitsString) < 1:
        raise Exception("No digits found")
    elif len(digitsString) == 1:
        return int(digitsString[0] + digitsString[0])
    else:
        return int(digitsString[0] + digitsString[-1])

# Read input1

input1 = open("input1", "r")

# Read line by line in a loop

sum = 0
for line in input1:
    sum += retrieveFirstAndLastDigit(line)

print(sum)
