def get_values(line) -> list[int]:
    line_content = line.split(":")[1]
    values = line_content.split()
    int_values = [int(x) for x in values]
    return int_values


def get_value_bad_kerning(line) -> int:
    line_content = line.split(":")[1]
    value = line_content.replace(" ", "")
    return int(value)


def get_margin(time: int, dist: int) -> int:
    # not optimized at all, I guess a linear equation and it's done !

    possible_ways_count = 0
    for holding_time in range(time):
        remaining_time_after_holding = time - holding_time
        speed = holding_time

        result_of_race = remaining_time_after_holding * speed

        if result_of_race > dist:
            possible_ways_count += 1

    return possible_ways_count


lines = open("input", "r").readlines()

# part 1
time_vals = get_values(lines[0])
dist_vals = get_values(lines[1])

race_records = [(time_vals[i], dist_vals[i]) for i in range(len(time_vals))]
print(race_records)

product = 1
for race_record in race_records:
    product *= get_margin(race_record[0], race_record[1])

print(product)
# part 1 was 220320


# part 2
race_time = get_value_bad_kerning(lines[0])
race_dist = get_value_bad_kerning(lines[1])

margin = get_margin(race_time, race_dist)

print(margin)
# part 2 was 34454850
