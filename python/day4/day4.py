

def read_numbers(number_string):
    numbers = []  # TODO would have liked to use list comprehension here, but issue with empty strings/split
    for x in number_string.strip().split(" "):
        if x.strip() != "":
            numbers.append(int(x.strip()))
    return numbers

### PART 1

input = open("input", "r")

total_score = 0

for l in input.readlines():
    card_id, content = l.split(":")

    win_part, chosen_part = content.split("|")

    win_numbers = read_numbers(win_part)
    chosen_numbers = read_numbers(chosen_part)

    this_card_score = 0
    for chosen_number in chosen_numbers:
        if chosen_number in win_numbers:
            if this_card_score == 0:
                this_card_score = 1
            else:
                this_card_score *= 2

    print("Card {} score is {}".format(card_id, this_card_score))
    total_score += this_card_score

print("Total score is {}".format(total_score))
# was 25004


### PART 2

input = open("input", "r")

copy_card_inventory = dict()

index = 1 # should correspond to card_id
for l in input.readlines():
    card_id, content = l.split(":")

    win_part, chosen_part = content.split("|")

    win_numbers = read_numbers(win_part)
    chosen_numbers = read_numbers(chosen_part)

    number_of_matches = 0
    for chosen_number in chosen_numbers:
        if chosen_number in win_numbers:
            number_of_matches += 1

    multiplier = copy_card_inventory.get(index, 0) + 1

    for i in range(index + 1, index + number_of_matches + 1):
        if i in copy_card_inventory:
            copy_card_inventory[i] += 1*multiplier
        else:
            copy_card_inventory[i] = 1*multiplier

    print("Card {} multiplier is {}, matches is {}".format(card_id, multiplier, number_of_matches))

    index += 1

score = index-1 # number of originals
for _, number in copy_card_inventory.items():
    score += number # +copies

print("Total score is {}".format(score))
